[![pipeline status](https://gitlab.com/sroman/sroman.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/sroman/sroman.gitlab.io/commits/master)

## Personal Site

This is just the repository where I store the files that are used to generate my personal website (which is hosted in Gitlab Pages).

To generate this website I use `Hugo`, a static site generator written in `Go`.

[hugo]: https://gohugo.io


