---
title: About me
comments: false
---

As it may be obvious already, my name is Santiago. I've enjoyed working with technology since I was little and I'm lucky enough to keep doing it to this day.

## What I do

 I work with infrastructure, my goal is always to allow customers (developers and consumers of an application or service) to not have to worry about it. Of course this wouldn't be possible without some sort of automation which over the past few years have made it essential to ensure I keep improving my software engineering skills. I like to think that my main goal is to always keep on learning new things that'll lead me to be closer to Google's concept of a Site Reliability Engineer.

Feel free to take a look at my [blog](https://nullch.ar), where I'll try to write about things I come up during my day to day activities which in my opinion aren't well known and could be useful to other people.

Other places to checkout what I'm currently doing are:

- [GitHub profile](https://github.com/santiagoroman)
- [GitLab profile](https://github.com/sroman)
